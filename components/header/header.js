

function Header() {
  const handleLogout = () => {
    localStorage.removeItem("token");
    window.location = "/userlogin";
  };
  return (
    <nav class="navbar navbar-expand-lg bg-body-tertiary" >
    <div class="container-fluid">
      <a class="navbar-brand" >Library</a>
      <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav me-auto mb-2 mb-lg-0">
          <li class="nav-item">
            <a class="nav-link active" aria-current="page" href="/home">Home</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="/findbook">Find Book</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="/addbook">Add Book</a>
          </li></ul>

        <ul class="navbar-nav d-flex">
                    <li class="nav-item">
                        <a class="nav-link" href="/userlogin">User Login</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="/adminlogin">Admin Login</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="/userregister">Register</a>
                    </li>
                </ul>
                <button class="logout-button" onClick={handleLogout}>Logout</button>
    
      </div>
    </div>
  </nav>
  
  );
}
export default Header;

  

