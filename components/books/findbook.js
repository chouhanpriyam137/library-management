import React, { useState, useEffect } from 'react';
import axios from 'axios';
import { Link } from 'react-router-dom';
import Header from '../header/header';

const Findbook = () => {
  const [books, setBooks] = useState([]);

  useEffect(() => {
    // Fetch the list of books from the API
    axios
      .get('http://127.0.0.1:4000/admin/booklist')
      .then((response) => {
        setBooks(response.data.data);
        console.log(response.data, 'Res Data');
      })
      .catch((error) => {
        console.error('Error fetching books:', error);
      });
  }, []);

  return (
    <>
      <Header />
      <div className="container mt-5">
        <h2>Book List</h2>
        <table className="table table-bordered table-striped">
          <thead>
            <tr>
              <th>Book Name</th>
              <th>Author</th>
              <th>Availability</th>
              <th>Action</th>
            </tr>
          </thead>
          <tbody>
            {books.map((book) => (
              <tr key={book._id}>
                <td>{book.bookname}</td>
                <td>{book.author}</td>
                <td>{book.availability}</td>
                <td>
                  <Link to={`/issuebook/${book._id}`}>
                    <button className="btn btn-primary">Issue Book</button>
                  </Link>
                </td>
              </tr>
            ))}
          </tbody>
        </table>
      </div>
    </>
  );
};

export default Findbook;
