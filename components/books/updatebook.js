import React, { useState, useEffect } from 'react';
import axios from 'axios';
import { useParams } from 'react-router-dom';
import { toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import { useNavigate  } from 'react-router-dom';

const UpdateBook = () => {
  const { id } = useParams();
  const [book, setBook] = useState({
    
    bookname: '',
    author: '',
    availability:""
    
  });
  const navigate = useNavigate(); 

  useEffect(() => {
   
    axios
      .get(`http://127.0.0.1:4000/admin/book/${id}`)
      .then((response) => {
        setBook(response.data.data); 
      })
      .catch((error) => {
        console.error('Error fetching product details:', error);
      });
  }, [id]);

  const handleInputChange = (event) => {
    const { name, value } = event.target;
    setBook((prevBook) => ({
      ...prevBook,
      [name]: value,
    }));
  };

  const handleUpdate = () => {
    
    axios
      .post(`http://127.0.0.1:4000/admin/updatebook/${book._id}`, book)
      .then((response) => {
        console.log('Book updated successfully:', response.data);
        toast.success('Book Updated successfully!', {
          onClose: () => {
           
            navigate('/booklist');
          },
        });

      })
      .catch((error) => {
        console.error('Error updating book:', error);

      });
  };

  return (
    <div className="container mt-5">
      <h2>Update Book</h2>
      <form>
        
        <div className="mb-3">
          <label className="form-label">Book Name</label>
          <input
            type="text"
            className="form-control"
            name="bookname"
            value={book.bookname}
            onChange={handleInputChange}
          />
        </div>
        <div className="mb-3">
          <label className="form-label">Author</label>
          <input
            type="text"
            className="form-control"
            name="author"
            value={book.author}
            onChange={handleInputChange}
          />
        </div>
        <div className="mb-3">
          <label className="form-label">Availability</label>
          <input
            type="text"
            className="form-control"
            name="availability"
            value={book.availability}
            onChange={handleInputChange}
          />
        </div>
        
        <button type="button" className="btn btn-primary" onClick={handleUpdate}>
          Update Book
        </button>
      </form>
    </div>
  );
};

export default UpdateBook;


