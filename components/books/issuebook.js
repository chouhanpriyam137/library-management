

import React, { useState, useEffect } from 'react';
import axios from 'axios';

const IssueBook = () => {
  const [issueDetails, setIssueDetails] = useState({
    bookId: '',
    bookName: '',
    issueDate: '',
    submissionDate: '',
  });

  const [bookList, setBookList] = useState([]);
  const [issuedBooks, setIssuedBooks] = useState([]);

  useEffect(() => {
    // Fetch the list of books from the API
    axios
      .get('http://127.0.0.1:4000/admin/booklist')
      .then((response) => {
        setBookList(response.data.data);
        console.log(response.data, 'Book List');
      })
      .catch((error) => {
        console.error('Error fetching books:', error);
      });

    // Fetch the list of currently issued books (assuming you have an API endpoint for this)
    axios
      .get('http://127.0.0.1:4000/admin/issuedbooks')
      .then((response) => {
        setIssuedBooks(response.data.data);
        console.log(response.data, 'Issued Books');
      })
      .catch((error) => {
        console.error('Error fetching issued books:', error);
      });
  }, []);

  const calculateSubmissionDate = (issueDate) => {
    if (!issueDate) {
      // Handle the case where issueDate is not set
      return '';
    }

    const submissionDate = new Date(issueDate);

    if (isNaN(submissionDate.getTime())) {
      // Handle the case where issueDate is not a valid date
      return '';
    }

    submissionDate.setDate(submissionDate.getDate() + 15);

    // Format the submission date as needed (e.g., YYYY-MM-DD)
    const formattedSubmissionDate = submissionDate.toISOString().split('T')[0];

    return formattedSubmissionDate;
  };

  const handleChange = (e) => {
    const selectedBookName = e.target.value;
    const selectedBook = bookList.find((book) => book.bookname === selectedBookName);

    setIssueDetails({
      ...issueDetails,
      bookId: selectedBook ? selectedBook._id : '',
      bookName: selectedBookName || '',
      [e.target.name]: e.target.value,
      submissionDate: calculateSubmissionDate(e.target.value),
    });
  };

  const handleSubmit = (e) => {
    e.preventDefault();

    // Handle the submission of the issued book details
    console.log('Issued Book Details:', issueDetails);
  };

  return (
    <div className="container mt-5">
      <h2>Issue Book</h2>
      <form onSubmit={handleSubmit}>
        <div className="mb-3">
          <label htmlFor="bookName" className="form-label">
            Book Name:
          </label>
          <select
            className="form-select"
            id="bookName"
            name="bookName"
            value={issueDetails.bookName}
            onChange={handleChange}
          >
            <option value="" disabled>
              Select a Book Name
            </option>
            {bookList.map((book) => (
              <option key={book._id} value={book.bookname}>
                {book.bookname}
              </option>
            ))}
          </select>
        </div>
        <div className="mb-3">
          <label htmlFor="issueDate" className="form-label">
            Date of Issue:
          </label>
          <input
            type="date"
            className="form-control"
            id="issueDate"
            name="issueDate"
            value={issueDetails.issueDate}
            onChange={handleChange}
          />
        </div>
        <div className="mb-3">
          <label htmlFor="submissionDate" className="form-label">
            Submission Date:
          </label>
          <input
            type="text"
            className="form-control"
            id="submissionDate"
            name="submissionDate"
            value={issueDetails.submissionDate}
            readOnly
          />
        </div>
        <button type="submit" className="btn btn-primary">
          Issue Book
        </button>
      </form>
    </div>
  );
};

export default IssueBook;
