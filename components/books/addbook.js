import "./addbook.css";
import {React,useState} from 'react';
import { toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import { useNavigate  } from 'react-router-dom';
import axios from 'axios';

function Addbook() {
    const [book, setBook] = useState({
       bookname:"",
       author:'',
       availability:""
      });
    
      const navigate = useNavigate(); 
    
      const handleChange = (event) => {
        const { name, value } = event.target;
        setBook((prevBook) => ({
          ...prevBook,
          [name]: value,
        }));
      };
    
      const handleSubmit = (event) => {
        event.preventDefault();
    
        axios
          .post('http://127.0.0.1:4000/admin/addbook', book)
          .then((response) => {
            console.log('Book added successfully:', response.data);
    
            toast.success('Book added successfully!', {
                onClose: () => {
                 
                  navigate('/booklist');
                },
              });
          })
          .catch((error) => {
            console.error('Error adding book:', error);
            toast.error('Error adding book. Please try again.');
          });
      };
  return (
    <>
    <div class="container">
    <div class="reservation-form">
        <h2>Add books</h2>
        <form onSubmit={handleSubmit} action="#" method="post">
           
           
            <div class="form-group">
                <label for="name">Book Name:</label>
                <input type="text" id="bookname" name="bookname" value={book.bookname}
            onChange={handleChange}required/>
            </div>
            <div class="form-group">
                <label for="">author:</label>
                <input type="" id="author" name="author" value={book.author}
            onChange={handleChange}required/>
            </div>
            <div class="form-group">
                <label for="">Availability:</label>
                <input type="" id="availability" name="availability" value={book.availability}
            onChange={handleChange}required/>
            </div>
            
            <div class="form-group">
                <button type="submit" className="btn btn-primary">Add Book</button>
                <div><br></br>
                <a className="btn btn-primary" href="/booklist" role="button"> See Book List</a>
            </div></div>
        </form>
    </div>
    </div>
    </>
  );
}

export default Addbook;
