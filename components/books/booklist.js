import React, { useState, useEffect } from 'react';
import axios from 'axios';
import { Link} from 'react-router-dom';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faEdit, faTrash } from '@fortawesome/free-solid-svg-icons';
import { toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import { useNavigate  } from 'react-router-dom';



const BookList = () => {
  const [books, setBooks] = useState([]);
  const navigate = useNavigate(); 

  useEffect(() => {
   
    axios
      .get('http://127.0.0.1:4000/admin/booklist')
      .then((response) => {
        setBooks(response.data.data);
        console.log(response.data,"Res Data")
      })
      .catch((error) => {
        console.error('Error fetching books:', error);
      });
  }, []);

  const handleDelete = (bookId) => {
   
    axios
      .delete(`http://127.0.0.1:4000/admin/deletebook/${bookId}`)
      .then(() => {
       
        setBooks((prevBooks) =>
          prevBooks.filter((book) =>  book._id !== bookId)
        );

        toast.success('Book deleted successfully!');
        navigate('/booklist');
      })
      .catch((error) => {
        console.error('Error deleting book:', error);
        toast.error('Error deleting book. Please try again.');
      });
  };

  return (
    <>
    
    <div className="container mt-5">
      <h2>Book List</h2>
      <table className="table table-bordered table-striped">
        <thead>
          <tr>
            <th>Book Name</th>
            <th>Author</th>
            <th>Availability</th>
            
          </tr>
        </thead>
        <tbody>
           
          {books.map((book) => (
            <tr key={book._id}>
              <td>{book.bookname}</td>
              <td>{book.author}</td>
              <td>{book.availability}</td>
              
              <td>
                <Link to={`/updatebook/${book._id}`}>
                  <FontAwesomeIcon icon={faEdit} className="me-2" />
                </Link>
                <FontAwesomeIcon
                  icon={faTrash}
                  className="text-danger"
                  style={{ cursor: 'pointer' }}
                  onClick={() => handleDelete(book._id)}
                />
              </td>
            </tr>
          ))}
        </tbody>
      </table>
    </div>
    </>
  );
};

export default BookList;











