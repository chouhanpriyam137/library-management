
import './App.css';
import React from 'react';
import { Routes, Route ,Navigate} from 'react-router-dom';

import Header from './components/header/header';
import Home from './components/home/home';
import Findbook from './components/books/findbook';
import Addbook from './components/books/addbook';
import BookList from './components/books/booklist';
import UpdateBook from './components/books/updatebook';
import IssueBook from './components/books/issuebook';
import Userregister from './components/login/userregister';
import UserLogin from './components/login/userlogin';
import AdminLogin from './components/login/adminlogin';
function App() {
  const user = localStorage.getItem("token");
  return (
    
  

    <div className="App">
      <Routes>
     <Route path='/' exact element={<Home/>}/>   
     <Route path="/header" exact element={<Header/>}/>
    <Route path="/findbook" exact element={<Findbook/>}/>
    <Route path="/addbook" exact element={<Addbook/>}/>
    <Route path="/booklist" exact element={<BookList/>}/>
    <Route path='/updatebook/:id' exact element={<UpdateBook/>}/>
    <Route path="/issuebook/:bookId" element={<IssueBook />} />

    <Route path="/userregister" exact element={<Userregister/>}/>
    <Route path="/" element={<Navigate replace to="/userlogin" />} />
    <Route path="/userlogin" exact element={<UserLogin/>}/>
    {user && <Route path="/" exact element={<Home />} />}BookList
    <Route path="/adminlogin" exact element={<AdminLogin/>}/>
     </Routes>
     
     
     
     </div>
    
     
  
  );
}

export default App;

